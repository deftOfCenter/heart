# OpenID HEART #

This repository contains the source files for the [OpenID Health Relationship Trust (HEART)](https://openid.net/wg/heart/) working group specifications. These specifications are in [xml2rfc](http://xml2rfc.tools.ietf.org/) format and must be compiled to be human-readable. 

Rendered versions are available on the [official HEART working repository page](http://openid.bitbucket.org/HEART/). The rendered versions are updated manually from the source repository, here.

To file a bug report or enhancement, use the [issue tracker](https://bitbucket.org/openid/heart/issues?status=new&status=open). 